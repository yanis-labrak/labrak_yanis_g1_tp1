package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static AnimalList animalList = new AnimalList();
    public RecyclerView animalRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAnimalRecyclerView();

//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 , animalList.getNameArray());

        /*animalListView.setAdapter(arrayAdapter);

        animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                goToAnimalActivity(item);
            }
        });*/
    }

    private void initAnimalRecyclerView() {

        animalRecyclerView = findViewById(R.id.animalRecyclerView);

        ArrayList<String> listNames = new ArrayList<String>();
        Collections.addAll(listNames, animalList.getNameArray());

        AnimalRecyclerAdapter adpater = new AnimalRecyclerAdapter(this, listNames);

        animalRecyclerView.setAdapter(adpater);
        animalRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
